# intro-to-spilo

How to install, configure, monitor, backup a herd of spilos.
Spilo's name derives from სპილო [spiːlɒ], the Georgian word for "elephant."

## References

* [Spilo: HA PostgreSQL Clusters with Docker](https://github.com/zalando/spilo)
* [Patroni: A Template for PostgreSQL HA with ZooKeeper, etcd or Consul](https://github.com/zalando/patroni)
* [Running PostgreSQL in Google Kubernetes Engine](https://www.redpill-linpro.com/techblog/2019/09/28/postgres-in-kubernetes.html)

## Monitor

* [pg_view: Postgres Real-Time Activity View Utility](https://github.com/zalando/pg_view)
